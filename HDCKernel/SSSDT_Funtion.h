#pragma once
#include "SSSDT.h"
__kernel_entry NTSYSCALLAPI NTSTATUS NtAllocateVirtualMemory(
	HANDLE    ProcessHandle,
	PVOID     *BaseAddress,
	ULONG_PTR ZeroBits,
	PSIZE_T   RegionSize,
	ULONG     AllocationType,
	ULONG     Protect
);


static HDC(APIENTRY *NtUserGetDC)(HWND hWnd) = NULL;

static BOOL(APIENTRY *NtGdiExtTextOutW)(IN HDC hDC,
    IN INT XStart,
	IN INT YStart,
	IN UINT fuOptions,
	IN OPTIONAL LPRECT UnsafeRect,
	IN LPWSTR UnsafeString,
	IN INT Count,
	IN OPTIONAL LPINT UnsafeDx,
	IN DWORD dwCodePage) = NULL;

static HWND
(APIENTRY* NtUserFindWindowEx)(HWND hwndParent,
	HWND hwndChildAfter,
	PUNICODE_STRING ucClassName,
	PUNICODE_STRING ucWindowName,
	DWORD dwUnknown) = NULL;

