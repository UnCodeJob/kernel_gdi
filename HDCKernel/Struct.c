#include "Struct.h"

ULONG64 GetGuiThread(PEPROCESS Pe)
{

	My_PEPROCESS Get;
	ULONG64 KtheadAddr = 0;
	ULONG64 ThreadAddr = 0;
	PKAPC ExitApc = NULL;

	Get = (My_PEPROCESS)Pe;

	ULONG64 entry = (ULONG64)Get->ThreadListHead.Flink;



	for (PLIST_ENTRY pLink = Get->ThreadListHead.Flink; pLink != (PLIST_ENTRY)&Get->ThreadListHead.Flink; pLink = pLink->Flink)
	{
		My_PETHREAD Et = CONTAINING_RECORD(pLink, My_ETHREAD, ThreadListEntry);

		if (Et->Tcb.Win32Thread != 0)
		{
			return (ULONG64)Et->Tcb.Win32Thread;
		}
	}

	return 0;

}