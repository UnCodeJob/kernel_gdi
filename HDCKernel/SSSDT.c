
#include "SSSDT.h"

PSYSTEM_SERVICE_TABLE g_KeServiceDescriptorTableShadow = NULL;

LONG GetShadowSSDTFuncIDByName(PCWSTR name)
{
	UNICODE_STRING BaseFuncName;
	UNICODE_STRING DestFuncName;
	LONG i = 0;
	RtlInitUnicodeString(&DestFuncName, name);
	for (i = 0; i < 830; i++)
	{
		RtlInitUnicodeString(&BaseFuncName, g_SSSDTTableName[i]);
		if (RtlEqualUnicodeString(&BaseFuncName, &DestFuncName, FALSE))
		{
			DbgPrint("---%d---", i);
			return i;
		}
	}
	return 1;
}

ULONGLONG GetKeServiceDescriptorTableShadow64()
{
	PUCHAR StartSearchAddress = (PUCHAR)__readmsr(0xC0000082);
	PUCHAR EndSearchAddress = StartSearchAddress + 0x500;
	PUCHAR i = NULL;
	UCHAR b1 = 0, b2 = 0, b3 = 0;
	ULONG templong = 0;
	ULONGLONG addr = 0;
	for (i = StartSearchAddress; i < EndSearchAddress; i++)
	{
		if (MmIsAddressValid(i) && MmIsAddressValid(i + 1) && MmIsAddressValid(i + 2))
		{
			b1 = *i;
			b2 = *(i + 1);
			b3 = *(i + 2);
			if (b1 == 0x4c && b2 == 0x8d && b3 == 0x1d) //4c8d1d
			{
				memcpy(&templong, i + 3, 4);
				addr = (ULONGLONG)templong + (ULONGLONG)i + 7;
				addr = addr + sizeof(SYSTEM_SERVICE_TABLE);
				return addr;
			}
		}
	}
	return 0;
}

VOID InitShadowHookSSDT()
{
	g_KeServiceDescriptorTableShadow = (PSYSTEM_SERVICE_TABLE)GetKeServiceDescriptorTableShadow64();
	if (g_KeServiceDescriptorTableShadow)
	{
	
	}
	else
	{
		DbgPrintEx(DPFLTR_IHVDRIVER_ID, 0, "获取ShadowSSDT内核表基址失败！\n");
	}
}

PVOID Get3SDTFunAddress(ULONG uIndex)
{
	//DbgBreakPoint();
	PVOID pRetAddr = NULL;
	PULONG W32pServiceTable = NULL;
	PSYSTEM_SERVICE_TABLE pKeServiceDescriptorTable = g_KeServiceDescriptorTableShadow;
	if (!pKeServiceDescriptorTable || uIndex > pKeServiceDescriptorTable->NumberOfServices) return NULL;

	W32pServiceTable = pKeServiceDescriptorTable->ServiceTableBase;
	if (!W32pServiceTable) return NULL;

	return (PVOID)(((LONG64)(W32pServiceTable[uIndex] >> 4) + (LONG64)W32pServiceTable) & 0xFFFFFFFF0FFFFFFF);
	
}
