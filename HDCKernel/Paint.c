#include "Paint.h"

NTSTATUS InitShadowSSSDT()
{
	NTSTATUS Status = STATUS_UNSUCCESSFUL;
	InitShadowHookSSDT();

	NtUserGetDC = (HDC(__cdecl *)(HWND))Get3SDTFunAddress(GetShadowSSDTFuncIDByName(L"NtUserGetDC"));
	if (NtUserGetDC == NULL) {
		DbgPrintEx(77, 0, "获取失败 NtUserGetDC\n");
		goto $EXIT;
	}

	NtGdiExtTextOutW = (BOOL(__cdecl *)(HDC, INT, INT, UINT, LPRECT, LPWSTR, INT, LPINT, DWORD))Get3SDTFunAddress(GetShadowSSDTFuncIDByName(L"NtGdiExtTextOutW"));
	if (NtGdiExtTextOutW == NULL) {
		DbgPrintEx(77, 0, "获取失败 NtGdiExtTextOutW\n");
		goto $EXIT;
	}

	NtUserFindWindowEx = (HWND(__cdecl *)(HWND, HWND, PUNICODE_STRING, PUNICODE_STRING, DWORD))Get3SDTFunAddress(GetShadowSSDTFuncIDByName(L"NtUserFindWindowEx"));
	if (NtUserFindWindowEx == NULL) {
		DbgPrintEx(77, 0, "获取失败 NtUserFindWindowEx\n");
		goto $EXIT;
	}


	Status = STATUS_SUCCESS;


$EXIT:
	return Status;
}

HDC GetDC(HWND hWnd)
{
	return NtUserGetDC(hWnd);
}



HWND FindWindow(WCHAR * lpClassName, WCHAR * lpWindowName)
{
	SIZE_T allocSize = sizeof(UNICODE_STRING) * 2 + 260 * sizeof(CHAR) * 2;
	PVOID pMem = NULL;
	NtAllocateVirtualMemory(NtCurrentProcess(),&pMem, 0, &allocSize, MEM_COMMIT, PAGE_READWRITE);
	RtlZeroMemory(pMem, allocSize);

	PUCHAR pBuffer = (PUCHAR)pMem;
	PUNICODE_STRING usClassName = (PUNICODE_STRING)(pBuffer);
	PUNICODE_STRING usWindowName = (PUNICODE_STRING)(pBuffer + sizeof(UNICODE_STRING));
	ULONG64 str_buffer = (ULONG64)pBuffer + sizeof(UNICODE_STRING) * 2;

	if (lpClassName)
	{
		RtlCopyMemory((PVOID)str_buffer, lpClassName, sizeof(WCHAR)*wcslen(lpClassName));
		RtlInitUnicodeString(usClassName, (PCWSTR)str_buffer);
		str_buffer += sizeof(WCHAR)*(wcslen(lpClassName) + 1);
	}
	if (lpWindowName)
	{
		RtlCopyMemory((PVOID)str_buffer, lpWindowName, sizeof(WCHAR)*wcslen(lpWindowName));
		RtlInitUnicodeString(usWindowName, (PCWSTR)str_buffer);
	}

	return NtUserFindWindowEx((HWND)0, (HWND)0, usClassName, usWindowName, (DWORD)0);
}

BOOL TextOutW(HDC hdc, int x, int y, LPCWSTR lpString, int c)
{
	PVOID pMem = NULL;
	SIZE_T allocSize = (c + 1) * sizeof(WCHAR);
	NtAllocateVirtualMemory(NtCurrentProcess(), &pMem, 0, &allocSize, MEM_COMMIT, PAGE_READWRITE);
	RtlZeroMemory(pMem, allocSize);
	RtlCopyMemory(pMem, lpString, c * sizeof(WCHAR));
	return NtGdiExtTextOutW(hdc, x, y, (UINT)0, (RECT *)NULL, (LPWSTR)pMem, c, (INT *)NULL, 0);
}

