#pragma once
#include "SSSDT_Funtion.h"
#include "Struct.h"
NTSTATUS InitShadowSSSDT();

HDC GetDC(_In_opt_ HWND hWnd);

HWND FindWindow(WCHAR * lpClassName, WCHAR * lpWindowName);

BOOL TextOutW(_In_ HDC hdc,_In_ int x,_In_ int y,_In_reads_(c) LPCWSTR lpString,_In_ int c);